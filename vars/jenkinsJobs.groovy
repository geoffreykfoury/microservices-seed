def call(appname){
    node {
        stage('Checkout') {
            checkout scm
        }

        // Execute different stages depending on the job
        if(env.JOB_NAME.contains("deploy")){
            envSetup()
            packageArtifact()
            dockerize(appname)
        } else if(env.JOB_NAME.contains("test")) {
            echo "Should not occur"
        }
    }
}

def packageArtifact(){
    stage('packaging') {
        sh "./mvnw verify -Pprod -DskipTests"
        archiveArtifacts artifacts: '**/target/*.war', fingerprint: true
    }
}

def envSetup(){
    stage('check java') {
        sh "java -version"
    }

    //clear target folder and past builds
    stage('clean') {
        sh "chmod +x mvnw"
        sh "./mvnw clean"
    }
}

def dockerize(appname){
    def dockerImage
    stage('build docker') {
        sh "cp -R src/main/docker target/"
        sh "cp target/*.war target/docker/"
        dockerImage = docker.build('gfkflyby/' + appname, 'target/docker')
    }

    stage('publish docker') {
        docker.withRegistry('https://registry.hub.docker.com', 'docker-login') {
            dockerImage.push 'latest'
        }
    }
}